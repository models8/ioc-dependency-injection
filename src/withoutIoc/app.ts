import { uploadSegmentGroups } from "./infrastructure/dependencies"

/**
 * Execute cache prefilling script
 * @returns void
 */
export async function run (): Promise<void> {
  await uploadSegmentGroups.execute()
}

run()
  .then(() => process.exit(0))
  .catch((error) => {
    process.exit(1)
  })
