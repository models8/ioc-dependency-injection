import { Chose } from "../../fakePackages/Chose/Chose"
import { CacheService } from "../../fakePackages/CacheService/CacheService"

export class BrandHistoryRepository {
  constructor (
    private chose: Chose,
    private cacheService: CacheService
  ) {}

  public async save () {
    await this.cacheService.salutation("saveThat")
    this.chose.ok()
    console.log("SAVE OK")
  }
  
}
