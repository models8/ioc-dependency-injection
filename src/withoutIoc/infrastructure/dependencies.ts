import { UploadSegmentGroups } from "../core/usecases/UploadSegmentGroups"
import { Chose } from "../fakePackages/Chose/Chose"
import { CacheService } from "../fakePackages/CacheService/CacheService"
import { Machin } from "../fakePackages/Machin/Machin"
import { NeedConfig } from "../fakePackages/NeedConfig/NeedConfig"
import { BrandHistoryRepository } from "../data/BrandHistoryRepository/BrandHistoryRepository"
import { VehicleJourney } from "../core/entities/vehicleJourney/VehicleJourney"
import { OkOkService } from "../core/services/OkOkService/OkOkService"
import { Axios } from "axios"
import Redis from "ioredis"
import { REDIS_CONFIG_BRAND_HISTORY, REDIS_CONFIG_CHOSE, REDIS_CONFIG_MACHIN } from "./config"

const axios : Axios = new Axios()
const redisClientMachin : Redis = new Redis(REDIS_CONFIG_MACHIN)
const redisClientChose : Redis = new Redis(REDIS_CONFIG_CHOSE)
const redisClientBrandHistory : Redis = new Redis(REDIS_CONFIG_BRAND_HISTORY)

export const vehicleJourneyEntity : VehicleJourney = new VehicleJourney()

export const cacheServiceMachin: CacheService = new CacheService(redisClientMachin)
export const cacheServiceChose: CacheService = new CacheService(redisClientChose)
export const cacheServiceBrandHistory: CacheService = new CacheService(redisClientBrandHistory)

export const needConfig: NeedConfig = new NeedConfig("test")
export const machin : Machin = new Machin(needConfig, axios, cacheServiceMachin)
export const chose : Chose = new Chose(machin, cacheServiceChose)

export const brandHistoryRepository : BrandHistoryRepository = new BrandHistoryRepository(
  chose,
  cacheServiceBrandHistory
)

export const okokService : OkOkService = new OkOkService(
  brandHistoryRepository,
  vehicleJourneyEntity,
  chose
)

export const uploadSegmentGroups : UploadSegmentGroups = new UploadSegmentGroups(
  okokService,
  machin
)
