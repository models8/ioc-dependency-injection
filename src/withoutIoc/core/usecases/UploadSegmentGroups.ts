import { OkOkService } from "../services/OkOkService/OkOkService"
import { okokService } from "../../infrastructure/dependencies"
import { Machin } from "../../fakePackages/Machin/Machin"

export class UploadSegmentGroups {
  constructor (
    private okokService: OkOkService,
    private machin: Machin
  ) {
  }

  public async execute () {
    await okokService.do()
    this.machin.makeSomething()
    return "gogo"
  }
}