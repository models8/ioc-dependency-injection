import { BrandHistoryRepository } from "../../../data/BrandHistoryRepository/BrandHistoryRepository"
import { VehicleJourney } from "../../entities/vehicleJourney/VehicleJourney"
import { Chose } from "../../../fakePackages/Chose/Chose"

export class OkOkService {
  
  constructor (
    private brandHistoryRepo: BrandHistoryRepository,
    private vehicleJourney: VehicleJourney,
    private chose: Chose
  ) {
  }

  public async do () {
    await this.brandHistoryRepo.save()
    this.vehicleJourney.doThing()
    this.chose.ok()
    console.log("do is DONE")
  }
}