import { Machin } from "../Machin/Machin"
import { CacheService } from "../CacheService/CacheService"

export class Chose {

  constructor (private machin: Machin, private cacheServiceChose: CacheService) {
  }

  public ok () {
    this.machin.makeSomething()
    this.cacheServiceChose.doNothing()
    console.log("ok")
  }

}