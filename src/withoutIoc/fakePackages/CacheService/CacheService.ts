import Redis from "ioredis"

export class CacheService {
  constructor (private redisClient: Redis) {
  }

  public async salutation (data: string) {
    await this.redisClient.set("fck" + Date.now(), data)
    console.log("hello")
  }

  public doNothing () {
    console.log("nothing")
  }
}