export class NeedConfig {

  config : string

  constructor (aString: string) {
    this.config = aString
    console.log(aString)
  }

  helloWorld () {
    console.log("hello config", this.config)
  }
}