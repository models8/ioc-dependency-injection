import { NeedConfig } from "../NeedConfig/NeedConfig"
import { CacheService } from "../CacheService/CacheService"
import { Axios } from "axios"

export class Machin {

  constructor (
    private needConfig: NeedConfig,
    private axios: Axios,
    private cacheServiceMachin: CacheService
  ) {
  }

  public makeSomething () {
    this.needConfig.helloWorld()
    this.cacheServiceMachin.doNothing()
    console.log("salut machin")
  }
  
}