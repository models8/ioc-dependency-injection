import { Machin } from "../Machin/Machin"
import { CacheService } from "../CacheService/CacheService"
import { inject, injectable } from "tsyringe"
import { IOC_KEYS } from "../../infrastructure/ioc/keys"

@injectable()
export class Chose {

  constructor (
    private machin: Machin,
    @inject(IOC_KEYS.cacheServiceB) private cacheServiceChose: CacheService) {
  }

  public ok () {
    this.machin.makeSomething()
    this.cacheServiceChose.doNothing()
    console.log("ok")
  }

}