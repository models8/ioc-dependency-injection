import { inject, injectable } from "tsyringe"
import { IOC_KEYS } from "../../infrastructure/ioc/keys"

@injectable()
export class NeedConfig {

  config : string

  constructor (@inject(IOC_KEYS.aString) aString: string) {
    this.config = aString
  }

  helloWorld () {
    console.log("hello config", this.config)
  }
}