import { NeedConfig } from "../NeedConfig/NeedConfig"
import { CacheService } from "../CacheService/CacheService"
import { inject, injectable } from "tsyringe"
import { Axios } from "axios"
import { IOC_KEYS } from "../../infrastructure/ioc/keys"

@injectable()
export class Machin {

  constructor (
    private needConfig: NeedConfig,
    @inject(IOC_KEYS.axios) private axios: Axios,
    @inject(IOC_KEYS.cacheServiceA) private cacheServiceMachin: CacheService
  ) {
  }

  public makeSomething () {
    this.needConfig.helloWorld()
    this.cacheServiceMachin.doNothing()
    console.log("salut machin")
  }
  
}