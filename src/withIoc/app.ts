import "reflect-metadata"
import { UploadSegmentGroups } from "./core/usecases/UploadSegmentGroups"
import { iocContainer } from "./infrastructure/ioc/dependencies"

/**
 * Execute cache prefilling script
 * @returns void
 */
export async function run (): Promise<void> {
  const uploadSegmentGroups : UploadSegmentGroups = iocContainer.resolve(UploadSegmentGroups)
  console.log("usecase")
  await uploadSegmentGroups.execute()
}

run()
  .then(() => {
    console.log("success !!")
    process.exit(0)
  })
  .catch((error) => {
    console.log("error !!", error)
    process.exit(1)
  })
