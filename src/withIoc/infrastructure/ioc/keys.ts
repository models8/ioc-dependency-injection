export const IOC_KEYS = {
  axios: Symbol.for("axios"),
  aString: Symbol.for("aString"),
  redisClient: Symbol.for("redisClient"),
  cacheServiceA: Symbol.for("cacheServiceA"),
  cacheServiceB: Symbol.for("cacheServiceB"),
  cacheServiceC: Symbol.for("cacheServiceC")
}
