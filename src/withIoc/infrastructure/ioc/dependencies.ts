import { container, predicateAwareClassFactory } from "tsyringe"
import { IOC_KEYS } from "./keys"
import { REDIS_CONFIG_CHOSE, REDIS_CONFIG_MACHIN } from "../config"
import { Axios } from "axios"
import { CacheService } from "../../fakePackages/CacheService/CacheService"
import Redis from "ioredis"

container.register(IOC_KEYS.cacheServiceA, { useValue: new CacheService(
  new Redis(REDIS_CONFIG_MACHIN)
) })
container.register(IOC_KEYS.cacheServiceB, { useValue: new CacheService(
  new Redis(REDIS_CONFIG_CHOSE)
) })
container.register(IOC_KEYS.cacheServiceC, { useValue: new CacheService(
  new Redis(REDIS_CONFIG_MACHIN)
) })

container.register(IOC_KEYS.aString, { useValue: "HELLO BOYS" })
container.register(IOC_KEYS.axios, { useValue: new Axios() })
predicateAwareClassFactory()
export { container as iocContainer }