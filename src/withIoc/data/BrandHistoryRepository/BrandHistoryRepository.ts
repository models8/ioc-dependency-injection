import { Chose } from "../../fakePackages/Chose/Chose"
import { CacheService } from "../../fakePackages/CacheService/CacheService"
import { inject, injectable } from "tsyringe"
import { IOC_KEYS } from "../../infrastructure/ioc/keys"

@injectable()
export class BrandHistoryRepository {
  constructor (
    private chose: Chose,
    @inject(IOC_KEYS.cacheServiceC) private cacheService: CacheService
  ) {}

  public async save () {
    await this.cacheService.salutation("saveThat")
    this.chose.ok()
    console.log("SAVE OK")
  }
  
}
