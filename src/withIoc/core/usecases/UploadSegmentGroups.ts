import { OkOkService } from "../services/OkOkService/OkOkService"
import { Machin } from "../../fakePackages/Machin/Machin"
import { injectable } from "tsyringe"

@injectable()
export class UploadSegmentGroups {
  constructor (
    private okokService: OkOkService,
    private machin: Machin
  ) {
  }

  public async execute () {

    await this.okokService.do()
    this.machin.makeSomething()
    return "gogo"
  }
}